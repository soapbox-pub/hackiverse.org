import Head from 'next/head';

export default function Home() {
  return (
    <>
      <Head>
        <title>Hackiverse 2022</title>
        <meta name='description' content="We're collaborating as a community to build the Fediverse! Join participating projects that are leading the way on decentralized social media, (no coding experience required). Find us on the Fediverse with #hackiverse." />
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no' />

        {/* Open Graph protocol */}
        <meta property='og:title' content='Hackiverse 2022' />
        <meta property='og:type' content='website' />
        <meta property='og:url' content='https://hackiverse.org' />
        <meta property='og:image' content='https://hackiverse.org/assets/images/og-image.png' />
        <meta property='og:description' content="We're collaborating as a community to build the Fediverse! Join participating projects that are leading the way on decentralized social media, (no coding experience required). Find us on the Fediverse with #hackiverse." />
        <meta property='og:locale' content='en_US' />

        <link rel='apple-touch-icon' sizes='180x180' href='/assets/icons/apple-touch-icon.png' />
        <link rel='icon' type='image/png' sizes='32x32' href='/assets/icons/favicon-32x32.png' />
        <link rel='icon' type='image/png' sizes='16x16' href='/assets/icons/favicon-16x16.png' />
        <link rel='manifest' href='/assets/icons/site.webmanifest' />
        <link rel='mask-icon' href='/assets/icons/safari-pinned-tab.svg' color='#3c7f72' />
        <link rel='shortcut icon' href='/assets/icons/favicon.ico' />
        <meta name='msapplication-TileColor' content='#faf1dd' />
        <meta name='msapplication-config' content='/assets/icons/browserconfig.xml' />
        <meta name='theme-color' content='#faf1dd' />
      </Head>

      <div className='main-layout'>
        <div className='header'>
          <h1>
            <span className='text-teal'>2</span>
            <span className='text-maroon'>0</span>
            <span className='text-orange'>2</span>
            <span className='text-mustard'>2</span>
            {' Hackiverse '}
            <img className='logo-stars' src='/assets/images/stars.svg' />
          </h1>
    
          <div className='subheader'>
            October 24&mdash;30, 2022
          </div>
        </div>

        <hr className='divider' />

        <p className='informational'>
          We're collaborating as a community to build
          the <a href='https://jointhefedi.com/' target='_blank'>Fediverse</a>!
          Join participating projects that are
          leading the way on decentralized social media,
          (no coding experience required). Find us on the
          Fediverse with <strong>#hackiverse</strong>.
        </p>

        <h2 className='text-orange'>Join!</h2>

        <p className='informational'>
          Get involved in planning and discussion! Join our community chatroom to start hacking!
        </p>

        <a className='button' href='https://chat.soapbox.pub/signup_user_complete/?id=s8yk9qh4ctgcxppn5omo1sobac' target='_blank'>
          Join the community
        </a>

        <div className='footer'>
          <a href='https://gitlab.com/soapbox-pub/hackiverse.org' target='_blank'>Source code</a>. CC BY-SA 4.0.
        </div>
      </div>
    </>
  );
}
