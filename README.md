# hackiverse.org

The official website for Hackiverse 2022.

## License

Licensed under [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
